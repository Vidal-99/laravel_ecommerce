@extends('catalog.layout')

@section('content')
<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
    <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
        <div class="relative overflow-x-auto text-white">
            @if (count($cart) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" class="px-6 py-3">Produit</th>
                        <th scope="col" class="px-6 py-3">Quantité</th>
                        <th scope="col" class="px-6 py-3">Prix</th>
                        <th scope="col" class="px-6 py-3">Total</th>
                        <th scope="col" class="px-6 py-3"></th>
                    </tr>
                </thead>
                <tbody>
                    @php $total = null @endphp
                    @foreach ($cart as $product)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td class="px-6 py-4">{{ $product['product']->title }}</td>
                        <td class="px-6 py-4">
                            <a href="{{ route('cart.increase', $product['product']->id )}}" class="inline-block text-gray-500 dark:text-gray-400 px-2 py-1 bg-gray-200 dark:bg-gray-700 rounded-md hover:bg-gray-300 dark:hover:bg-gray-600">+</a>
                            <span class="px-2">{{ $product['quantity'] }}</span>
                            <a href="{{ route('cart.decrease', $product['product']->id )}}" class="inline-block text-gray-500 dark:text-gray-400 px-2 py-1 bg-gray-200 dark:bg-gray-700 rounded-md hover:bg-gray-300 dark:hover:bg-gray-600">-</a>
                        </td>
                        <td class="px-6 py-4">{{ number_format($product['product']->price / 1, 2, ',', '.') }}€</td>
                        <td class="px-6 py-4">{{ number_format(($product['product']->price * $product['quantity']) / 1, 2, ',', '.') }}€</td>
                        <td class="px-6 py-4">
                            <a href="{{ route('cart.delete', ['id' => $product['product']->id]) }}" class="text-red-500 hover:text-red-700">supp</a>
                        </td>
                    </tr>
                    @php $total += $product['product']->price * $product['quantity'] @endphp
                    @endforeach
                </tbody>
            </table>
            <div class="text-right mb-5 text-white">
                <b>Nombre de produit :</b> {{ count($cart) }} <br>
                <b>Total de mon panier :</b> {{ number_format($total / 1, 2, ',', '.') }}€ <br>
                <a href="" class="btn btn-success btn-block mt-3">Valider mon panier</a>
            </div>
            @else
            <hr>
            <p><b>Votre panier est vide</b></p>
            @endif
        </div>
    </div>
</div>
@endsection