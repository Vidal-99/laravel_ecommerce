@extends('products.layout')

@section('content')
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <a href="{{route('products.index')}}" class="inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border border-transparent rounded-md font-semibold text-xs text-white dark:text-gray-800 uppercase tracking-widest hover:bg-gray-700 dark:hover:bg-white focus:bg-gray-700 dark:focus:bg-white active:bg-gray-900 dark:active:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:focus:ring-offset-gray-800 transition ease-in-out duration-150">
                    {{ __('Retour') }}
                </a>
            </div>
        </div>
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <form action="{{ route('products.update', $product->id) }}" method="POST" class="mt-6 space-y-6">
                    @csrf
                    @method('PUT')

                    <div>
                        <label for="title" class="block font-medium text-sm text-gray-700 dark:text-gray-300">Title:</label>
                        <x-text-input type="text" name="title" id="title" value="{{ $product->title }}" class="block mt-1 w-full" />
                    </div>

                    <div class="mt-4">
                        <label for="description" class="block font-medium text-sm text-gray-700 dark:text-gray-300">Description:</label>
                        <textarea name="description" id="description" rows="4" class="block mt-1 w-full">{{ $product->description }}</textarea>
                    </div>

                    <div class="mt-4">
                        <label for="price" class="block font-medium text-sm text-gray-700 dark:text-gray-300">Price:</label>
                        <x-text-input type="number" name="price" id="price" value="{{ $product->price }}" class="block mt-1 w-full" step="0.01" />
                    </div>

                    <div class="mt-4">
                        <label for="quantity" class="block font-medium text-sm text-gray-700 dark:text-gray-300">Quantity:</label>
                        <x-text-input type="number" name="quantity" id="quantity" value="{{ $product->quantity }}" class="block mt-1 w-full" />
                    </div>

                    <div class="mt-8">
                        <button type="submit" class="inline-flex items-center px-4 py-2 bg-blue-500 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-blue-600 active:bg-blue-700 focus:outline-none focus:border-blue-900 focus:ring focus:ring-blue-200 disabled:opacity-25 transition">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection