<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CatalogController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;

Route::get('/', function () {
    return view('default');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware('auth')->resource('products', ProductController::class);
// Route::middleware('auth')->resource('cart', CartController::class);

Route::resource('catalog', CatalogController::class);
Route::post('/catalog/add/{id}', [CatalogController::class, 'add'])->name('catalog.add');
Route::post('/catalog', [CatalogController::class, 'index'])->name('catalog');

Route::get('/cart', [CartController::class, 'index'])->name('cart');
Route::get('/cart/increase/{id}', [CartController::class, 'increase'])->name('cart.increase');
Route::get('/cart/decrease/{id}', [CartController::class, 'decrease'])->name('cart.decrease');
Route::get('/cart/delete/{id}', [CartController::class, 'delete'])->name('cart.delete');


require __DIR__.'/auth.php';
