<?php

namespace App\Http\Services;

use App\Models\Product;
use Illuminate\Http\Request;

class CartService
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function add($id)
    {
        $cart = $this->request->session()->get('cart', []);
        if (!empty($cart[$id])) {
            $cart[$id]++;
        } else {
            $cart[$id] = 1;
        }

        $this->request->session()->put('cart', $cart);
    }

    public function remove()
    {
        return $this->request->session()->forget('cart');
    }

    public function get()
    {
        return $this->request->session()->get('cart', []);
    }

    public function delete($id)
    {
        $cart = $this->request->session()->get('cart', []);
        unset($cart[$id]);
        $this->request->session()->put('cart', $cart);
    }

    public function decrease($id)
    {
        $cart = $this->request->session()->get('cart', []);
        if ($cart[$id] > 1) {
            $cart[$id]--;
        } else {
            unset($cart[$id]);
        }
        $this->request->session()->put('cart', $cart);
    }

    public function increase($id)
    {
        $cart = $this->request->session()->get('cart', []);
        if ($cart[$id] > 1) {
            $cart[$id]++;
        } else {
            unset($cart[$id]);
        }
        $this->request->session()->put('cart', $cart);
    }

    public function getFull()
    {
        $cartComplete = [];
        if ($this->get()) {
            foreach ($this->get() as $id => $quantity) {
                $product = Product::find($id);
                if (!$product) {
                    $this->delete($id);
                    continue;
                }

                $cartComplete[] = [
                    'product' => $product,
                    'quantity' => $quantity
                ];
            }
        }
        return $cartComplete;
    }
}
