<?php

namespace App\Http\Controllers;

use App\Http\Services\CartService;

class CartController extends Controller
{
    public function __construct(private CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $cart = $this->cartService->getFull();

        return view('cart.index', compact('cart'));
    }

    public function remove()
    {
        $this->cartService->remove();
        return redirect()->route('product.index');
    }

    public function delete($id)
    {
        $this->cartService->delete($id);
        return redirect()->route('catalog.index');
    }

    public function decrease($id)
    {
        $this->cartService->decrease($id);
        return redirect()->route('cart');
    }
    
    public function increase($id)
    {
        $this->cartService->increase($id);
        return redirect()->route('cart');
    }
}
