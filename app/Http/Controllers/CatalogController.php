<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Services\CartService;

class CatalogController extends Controller
{
    public function __construct(private CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $products = Product::latest()->paginate(5);
        $cart = $this->cartService->getFull();

        return view('catalog.index', compact('products', 'cart'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function add($id)
    {
        $this->cartService->add($id);
        return redirect()->route('catalog.index');
    }

    
}
